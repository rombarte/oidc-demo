<?php

use App\OIDCClient;

require_once "../../vendor/autoload.php";

$client = new OIDCClient();
$user = $client->getUser($_GET['code'] ?? '');

$user->toArray();

var_dump($user);
