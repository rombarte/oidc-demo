# OPEN ID CONNECT PHP DEMO

## How to

```shell
  docker-compose up -d
```

## Create certificates

```sh
  openssl req -x509 -out /tmp/cert/ssl.crt -keyout /tmp/cert/ssl.key \
    -newkey rsa:2048 -nodes -sha256 -days 365 \
    -subj '/CN=localhost' -extensions EXT -config <( \
      printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")

  openssl pkcs12 -export -out /tmp/cert/ssl.pfx -inkey /tmp/cert/ssl.key -in /tmp/cert/ssl.crt
```

## License

Code is distributed on MIT License.