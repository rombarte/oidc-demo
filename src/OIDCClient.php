<?php

namespace App;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;

class OIDCClient
{
    private GenericProvider $OAuthProvider;
    private PKCEGenerator $PKCEGenerator;

    public function __construct()
    {
        $this->PKCEGenerator = new PKCEGenerator();
        $this->OAuthProvider = new GenericProvider(
            [
                'clientId' => 'clientId',
                'clientSecret' => 'clientSecret',
                'redirectUri' => "http://localhost/public/dashboard",
                'urlAuthorize' => 'https://localhost:90/connect/authorize',
                'urlAccessToken' => 'https://oidc-server:443/connect/token',
                'urlResourceOwnerDetails' => 'https://oidc-server:443/connect/userinfo',
            ]
        );
        $guzzleClient = new Client(
            [
                'defaults' => [
                    RequestOptions::CONNECT_TIMEOUT => 5,
                    RequestOptions::ALLOW_REDIRECTS => true
                ],
                RequestOptions::VERIFY => false,
            ]
        );
        $this->OAuthProvider->setHttpClient($guzzleClient);
    }

    public function login(): void
    {
        $PKCE = $this->PKCEGenerator->generate();
        $this->OAuthProvider->authorize(
            [
                'code_verifier' => $PKCE[0],
                'code_challenge' => $PKCE[1],
                'code_challenge_method' => $PKCE[2],
                'scope' => 'openid email profile',
            ]
        );
    }

    public function getUser(string $code): ResourceOwnerInterface
    {
        $PKCE = $this->PKCEGenerator->generate();
        $accessToken = $this->OAuthProvider->getAccessToken(
            'authorization_code',
            [
                'code' => $code,
                'code_verifier' => $PKCE[0],
                'code_challenge' => $PKCE[1],
                'code_challenge_method' => $PKCE[2],
            ]
        );

        return $this->OAuthProvider->getResourceOwner($accessToken);
    }

}