<?php

namespace App;

class PKCEGenerator
{
    public function generate(): array
    {
        $random = bin2hex(openssl_random_pseudo_bytes(32));
        $verifier = $this->base64urlEncode(pack('H*', $random));
        $challenge = $this->base64urlEncode(pack('H*', hash('sha256', $verifier)));
        $method = 'S256';

//        $verifier = 'elwQ6b4BRS4vggSUydVKZji8iudk4CcsQ02dF71L6l4';
//        $challenge = 'xdLygJ3TQ09YeubRNKoHHIEjPHf2jefMrX9zvz2vhtM';

        return [$verifier, $challenge, $method];
    }

    private function base64urlEncode(string $text): string
    {
        $base64 = base64_encode($text);
        $base64 = trim($base64, "=");

        return strtr($base64, '+/', '-_');
    }
}