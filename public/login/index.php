<?php

use App\OIDCClient;

require_once "../../vendor/autoload.php";

$client = new OIDCClient();
$client->login();